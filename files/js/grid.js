
$(document).ready(function() {
    $("#main-menu-ection").click(function (){
        height = $(".main-menu-holder .main-menu nav").height();        
        if($(this).parents(".main-menu-holder").hasClass("active")){
            $(".main-menu-holder .main-menu").height(0);
            $(this).parents(".main-menu-holder").removeClass("active")
        }  
        else{
            $(".main-menu-holder .main-menu").height(height);
            $(this).parents(".main-menu-holder").addClass("active")
        }      
    });
    /*******************************************/ 
    $(".main-menu ul li a").click(function (){        
        $("#nav-full").removeClass("active");
        $("#nav-full #main-menu-ection").removeClass("active");          
    });  
});
$(document).ready(function() { 
    var height_base = $(".hamburger .item.active").children(".text").children(".text-wrapper").height();
    $(".hamburger .item.active .text").height(height_base);    
    $(".hamburger .item").click(function (){
        height = $(this).children(".text").children(".text-wrapper").height();        
        if($(this).hasClass("active")){}  
        else{
            $(".hamburger .item").removeClass("active")
            $(".hamburger .item  .text").height(0);
            $(this).addClass("active")
            $(".hamburger .item.active .text").height(height);            
        }      
    });    
});

$(document).ready(function() {    
    function noScroll(){
        var display_w = window.innerWidth;  
        if(display_w > 737){    
            $("body").addClass('modall-active');
        }     
    };
    function yesScroll(){
       $("body").removeClass('modall-active');  
       $(".modall").removeClass('modall-open');        
    }
    /*******************************************/
    function modalDim(){ 
        $(".modall-open").fadeIn();
        var display_w = window.innerWidth;  
             
        if(display_w > 737){                
            var display_h = window.innerHeight;
            var modal_h = $(".modall-open .modall-block").height();
            var position_top = display_h - modal_h;
            var position_top = position_top/2.5;
            $(".modall-open .modall-block").css("top", position_top);            
        }
        else{
            var height1 = $(".wrapper").height();
            var height2 = $("footer").height();
            $(".modall").height(height1 + height2);        
            var position_top = window.pageYOffset+30;
            $(".modall-open .modall-block").css("top", position_top);             
        } ;         
    };
    /*******************************************/ 
    $(window).resize(function() {
        modalDim();       
    });
    $(document).keyup(function(e) {     
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            $(".modall").fadeOut();  
            $(".modall-open .modall-block").css("top", "10vh"); 
            yesScroll(); 
        }
    });
    $(".modall-close").click(function (){
        $(".modall").fadeOut();  
        $(".modall-open .modall-block").css("top", "10vh"); 
        yesScroll();    
    });
    $(".modall_fon").click(function (){
        $(".modall").fadeOut();   
        $(".modall-open .modall-block").css("top", "10vh"); 
        yesScroll();    
    });
     $(".privacy_policy").click(function (){         
        noScroll();
        $("#privacy_policy").fadeIn();          
    }); 
    $(".back-call").click(function (){ 
        $("#back-call").addClass('modall-open');
        modalDim();
        noScroll();       
    });
    $(".simple-order").click(function (){ 
        $("#simple-order").addClass('modall-open');
        modalDim();
        noScroll();       
    });
    
    
    
    $(".order_btn").click(function (){ 
        $("#order_modal_form").addClass('modall-open');
        modalDim();
        noScroll();       
    });
});
/****************************************/