

$(document).ready(function() {
    /*******************************************/ 
    var doc_width = $(window).width();
    if(doc_width < 769){} 
    var h_hght = 250;      
    var h_mrg = 0;                       
    $(function(){         
        var elem = $('#fixed-top-line');            
        var top = $(this).scrollTop();             
        if(top > h_hght){
            elem.css({'position' : 'fixed'});
            $('#nav-full').addClass("fixed");
        }                        
        $(window).scroll(function(){
            top = $(this).scrollTop();                 
            if (top+h_mrg < h_hght) {                
                $('#fixed-top-line').removeClass("fixed");
                
            } else {                
                $('#fixed-top-line').addClass("fixed");
                
            }
        });         
    });    
    /*******************************************/  
});

$(document).ready(function() {
    $('.reviews-slick').slick({
        dots: true,       
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,       
    }); // front-slick 
    
});

/****************************************/
/****************************************/
$(document).ready(function() {
//------------ remove error  -----
    $("input[type=text]").click(function (){      
        $(this).removeClass("error");
        $(this).parents("form").removeClass("stop");
        $(this).nextAll(".error_block_lendth").css("display", "none");
        $(this).nextAll(".error_block_corect").css("display", "none");
    });
//------------ remove error END -----
//------------ form send  -----
    $("form").bind("submit", function(event) {         
        $(this).nextAll('.progres').css('display', "block");
        $(this).addClass("sending"); 
        /**********  �������� �� ���������� ����� ***************/
            $(this).children('.form-item').children('input.mandatory').each(function() {                  
                if(!$(this).val().length) {                
                    $(this).addClass("error");                    
                    $(this).nextAll(".error_block_lendth").css("display", "block");
                    $(this).parents("form").addClass("stop"); 
                    $('form.stop').nextAll(".updateProgress_wrapp").css('display', "none");                    
                    event.preventDefault();                                 
                }                                                                       
            });
        /**********  �������� �� ����������� ���� ������� ********** *****/      
            $(this).children('.form-item').children('input.phone').ready(function() {
                if(/^([+0-9\s]{10,19})$/.test($(this).val())) return ;
                else{                              
                    $((this)).addClass("error");                    
                    $(this).nextAll(".error_block_corect").css("display", "block");
                    $(this).parents("form").addClass("stop"); 
                    $('form.stop').nextAll(".updateProgress_wrapp").css('display', "none");                                           
                    event.preventDefault(); 
                }                            
            });              
        /**********  �������� �� ����������� ���� E-mail ********** *****/
            function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);}  
                  
            $(this).children('.form-item').children('input.mail').each(function() {
                var email = $(this).val();                               
                if(isValidEmailAddress(email)){} 
                else{                    
                    $((this)).addClass("error");
                    $(this).nextAll(".error_block_corect").css("display", "block");
                    $(this).parents("form").addClass("stop"); 
                    $('form.stop').nextAll(".updateProgress_wrapp").css('display', "none");                                                
                    event.preventDefault(); 
                }                   
            }); 
        /***********************************************************/             
        if($(this).hasClass("stop")){$('form.stop').nextAll('.progres').css('display', "none");}
        else{             
            var msg   = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: 'order.php',
                data: msg,
                success: function(data) {                    
                    $(".sending").parents(".form-box-content").addClass("formsend"); 
                    $(".formsend .results").css("display", "block");
                    $(".sending").css("display", "none"); 
                    $('form.sending').nextAll(".updateProgress_wrapp").css('display', "none");        
                    $("form.sending").removeClass("sending");                                                                 
                },
                error:  function(xhr, str){
                    alert('�������� ������: ' + xhr.responseCode);
                }
            });                                             
        }
        /******************** ����� ���������� ***************************************/
    });       
});//------------ forms END -----